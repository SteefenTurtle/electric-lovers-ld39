﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Level_Complete : MonoBehaviour
{
    public GameObject sweatManager;

    public bool level1;

    // Use this for initialization
    void Start()
    {
        sweatManager = GameObject.Find("SweatManager");

    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            col.SendMessage("LevelDone", SendMessageOptions.DontRequireReceiver);
            sweatManager.SendMessage("LevelDone", SendMessageOptions.DontRequireReceiver);
            col.transform.Find("Plug").GetComponent<Animator>().SetBool("levelComplete", true);
            StartCoroutine(LevelComplete());
        }
    }

    IEnumerator LevelComplete()
    {
        yield return new WaitForSeconds(.7f);
        FindObjectOfType<Audio_Manager>().Play("LevelFinished");
        yield return new WaitForSeconds(4.5f);

        if(level1)
        {
            SceneManager.LoadSceneAsync("Level2");
        }
        else
        {
            SceneManager.LoadSceneAsync("StartScreen");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sweat_Manager : MonoBehaviour
{
    public float sweatDelay = 1f;
    public float launchSpeed = 1f;

    private bool canSweat = true;
    private bool levelDone = false;

    private int sweatCount = 0;

    public GameObject sweatPrefab;
    public GameObject heartPrefab;
    public GameObject lightningPrefab;
    private GameObject sweatSpawner;

    private Player_Movement playerMovementScript;

    // Use this for initialization
    void Start()
    {
        sweatSpawner = GameObject.Find("SweatSpawner");
        playerMovementScript = FindObjectOfType<Player_Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        SweatTimer();
    }

    void SweatTimer()
    {
        if (canSweat && !levelDone)
        {
            canSweat = false;
            Invoke("LaunchSweat", sweatDelay);
            Invoke("LaunchSweat", sweatDelay);
        }
    }

    void LaunchSweat()
    {
        Quaternion randomRotation = Quaternion.Euler((Random.Range(60, 135)), 0, 0);

        GameObject sweatInstance = Instantiate(sweatPrefab, sweatSpawner.transform.position, randomRotation);

        Vector3 forceDir = -sweatInstance.transform.forward;

        sweatInstance.GetComponent<Rigidbody>().AddForce(forceDir * launchSpeed);

        Vector3 forceAdj = playerMovementScript.movementSpeed * 0.07f;
        forceAdj.y = 0;

        sweatInstance.GetComponent<Rigidbody>().AddForce(forceAdj * launchSpeed);

        if (!levelDone)
        {
            canSweat = true;
        }
    }

    public void LevelDone()
    {
        levelDone = true;
        Invoke("HeartsSpread", 1.2f);
        Invoke("HeartsSpread", 1.2f);
        Invoke("HeartsSpread", 1.2f);
    }

    void HeartsSpread()
    {
        Quaternion randomRotation = Quaternion.Euler((Random.Range(-60, -135)), 0, 0);

        GameObject heartInstance = Instantiate(heartPrefab, sweatSpawner.transform.position, randomRotation);

        Vector3 forceDir = heartInstance.transform.forward;

        heartInstance.GetComponent<Rigidbody>().AddForce(forceDir * launchSpeed * 0.75f);
    }

    public void Zapped()
    {
        LightningSpread();
        LightningSpread();
    }

    void LightningSpread()
    {
        Quaternion randomRotation = Quaternion.Euler((Random.Range(-80, -110)), 0, 0);

        GameObject BoltInstance = Instantiate(lightningPrefab, sweatSpawner.transform.position, randomRotation);

        Vector3 forceDir = BoltInstance.transform.forward;

        BoltInstance.GetComponent<Rigidbody>().AddForce(forceDir * launchSpeed * 0.4f);
    }
}

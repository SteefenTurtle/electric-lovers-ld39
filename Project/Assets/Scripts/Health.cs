﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Health : MonoBehaviour
{
    private Text healthText;
    private Text deathMessage;
    public int maxHealth = 100;
    private int health = 0;
    private int damage = 20;


    private Vector3 lastCheckpoint = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        health = maxHealth;
        healthText = GameObject.Find("Health").GetComponent<Text>();
        deathMessage = GameObject.Find("DeathMessage").GetComponent<Text>();

        deathMessage.text = null;

        healthText.text = health.ToString();
        healthText.color = Color.green;
    }

    public void Damage()
    {
        FindObjectOfType<Audio_Manager>().Play("Zap");
        FindObjectOfType<Sweat_Manager>().Zapped();

        health -= damage;
        healthText.text = health.ToString();

        if(health >= 70)
        {
            healthText.color = Color.green;
        }
        else if(health >= 40)
        {
            healthText.color = Color.yellow;
        }
        else
        {
            healthText.color = Color.red;
        }

        if(health <= 0)
        {
            Death();
                
        }
    }


    public void ReceiveCheckpointInfo(Vector3 checkpoint)
    {
        lastCheckpoint = checkpoint;
    }

    public void Death()
    {
        StartCoroutine(Deathmessage());

        transform.position = lastCheckpoint;
        health = maxHealth;
        healthText.text = health.ToString();
        healthText.color = Color.green;

    }

    IEnumerator Deathmessage()
    {
        int randomText = Random.Range(0, 5);

        switch (randomText)
        {
            case 0:
                deathMessage.text = "Forgot your umbrella?";
                break;

            case 1:
                deathMessage.text = "Water is dangerous you know";
                break;

            case 2:
                deathMessage.text = "You're still not waterproof!";
                break;

            case 3:
                deathMessage.text = "Try a raincoat next time!";
                break;

            case 4:
                deathMessage.text = "That was a 'shocking' ending";
                break;

            case 5:
                deathMessage.text = "Who said machines can't swim";
                break;

            default:
                break;
        }

        yield return new WaitForSeconds(2f);

        deathMessage.text = null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plug_Hit_Tint : MonoBehaviour
{
    public Material plugMaterial;
    public Material plugHitMaterial;

    public void CallDamageTint()
    {
        StartCoroutine(DamageTint());

    }

    public IEnumerator DamageTint()
    {
        GetComponent<Renderer>().material = plugHitMaterial;

        yield return new WaitForSeconds(.1f);

        GetComponent<Renderer>().material = plugMaterial;
    }
}

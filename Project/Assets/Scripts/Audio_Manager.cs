﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class Audio_Manager : MonoBehaviour {

    public Sound[] sounds;

    public static Audio_Manager instance;

    void Awake()
    {

        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }    
    }

    void Start()
    {
        ThemeMusic();    
    }

    void ThemeMusic()
    {
        Play("Rain");
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            return;

        s.source.Play();
    }
}

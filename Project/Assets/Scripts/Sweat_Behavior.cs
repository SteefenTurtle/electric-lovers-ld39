﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sweat_Behavior : MonoBehaviour
{
    private bool isDangerous = false;

    private GameObject plugObject;

    // Use this for initialization
    void Start()
    {
        plugObject = GameObject.Find("Plug");
        Invoke("BecomesDangerous", 0.5f);
    }

    void BecomesDangerous()
    {
        isDangerous = true;
        GetComponent<Rigidbody>().AddTorque(Vector3.right * 90);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.transform.CompareTag("Floor"))
        {
            Destroy(gameObject);
        }
        else if (col.transform.CompareTag("Player"))
        {
            if (isDangerous)
            {
                Destroy(gameObject);
                col.BroadcastMessage("Damage", SendMessageOptions.DontRequireReceiver);

                plugObject.SendMessage("CallDamageTint", SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlaySelect : MonoBehaviour
{
    public LayerMask signMask;

    // Update is called once per frame
    void Update()
    {
        SelectPlay();
    }

    void SelectPlay()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit;

            if (Physics.Raycast(ray, out rayHit, 50f, signMask))
            {
                if (rayHit.transform.CompareTag("Sign"))
                {
                    SceneManager.LoadSceneAsync("level1", LoadSceneMode.Single);
                }
            }
        }
    }
}

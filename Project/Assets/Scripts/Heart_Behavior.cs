﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart_Behavior : MonoBehaviour
{
    private bool isDangerous = false;

    private void Start()
    {
        Invoke("BecomesDangerous", 0.5f);
    }

    void BecomesDangerous()
    {
        isDangerous = true;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.transform.CompareTag("Floor"))
        {
            Destroy(gameObject);
        }
        else if (col.transform.CompareTag("Player"))
        {
            if (isDangerous)
            {
                Destroy(gameObject);

            }
        }
    }
}

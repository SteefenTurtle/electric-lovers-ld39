﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    private CharacterController controller;

    public float Speed = 1f;
    public float jumpHeight = 1f;
    public float gravity = 1f;

    private float verticalSpeed = 0f;

    private bool canWalkSound = true;

    public GameObject plug;

    [HideInInspector]
    public Vector3 movementSpeed = Vector3.zero;

    private bool canMove = true;

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        WalkAnimation();
    }

    void Movement()
    {
        if (canMove)
        {
            movementSpeed = new Vector3(0, 0, Input.GetAxis("Horizontal"));
            movementSpeed = transform.TransformDirection(movementSpeed);
            movementSpeed *= Speed;
        }
        
        if (controller.isGrounded)
        {
            verticalSpeed = 0;
            
            if (Input.GetButton("Jump") && canMove)
            {
                FindObjectOfType<Audio_Manager>().Play("Jump");

                verticalSpeed = jumpHeight;
                StartCoroutine(JumpAnimaton());
            }

        }
        verticalSpeed -= gravity * Time.deltaTime;
        movementSpeed.y = verticalSpeed;

        controller.Move(movementSpeed * Time.deltaTime);
    }

    IEnumerator JumpAnimaton()
    {
        plug.GetComponent<Animator>().SetBool("isJumping", true);

        yield return new WaitForSeconds(0.2f);

        plug.GetComponent<Animator>().SetBool("isJumping", false);
    }

    void WalkAnimation()
    {
        if(Input.GetAxis("Horizontal") != 0)
        {
            plug.GetComponent<Animator>().SetBool("isWalking", true);
            StartCoroutine(WalkSounds());
        }
        else
        {
            plug.GetComponent<Animator>().SetBool("isWalking", false);
        }
    }

    public void LevelDone()
    {
        movementSpeed.x = 0;
        movementSpeed.z = 0;
        canMove = false;
    }

    IEnumerator WalkSounds()
    {
        if (canWalkSound && controller.isGrounded)
        {
            yield return new WaitForSeconds(.25f);

            canWalkSound = false;
            FindObjectOfType<Audio_Manager>().Play("Walk");

            yield return new WaitForSeconds(.25f);
            canWalkSound = true;
        }
        
    }
}

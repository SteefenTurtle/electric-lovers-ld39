﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoints : MonoBehaviour
{
    private Vector3 spawnPoint = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        spawnPoint = transform.Find("Spawn").transform.position;

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            col.SendMessage("ReceiveCheckpointInfo", spawnPoint);

            if (transform.Find("Flag") != null)
            {
                transform.Find("Flag").GetComponent<Animator>().SetTrigger("Checkpoint");
                FindObjectOfType<Audio_Manager>().Play("Swoosh");
            }
        }
    }
}
